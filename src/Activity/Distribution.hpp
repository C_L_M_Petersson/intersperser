#ifndef ACTIVITY_DISTRIBUTION_HPP
#define ACTIVITY_DISTRIBUTION_HPP

#include "Activity.hpp"


void calculateProbabilities();
std::string randomActivityIndex();
Activity* activityFromOption( std::string, bool&, bool& );

#endif
