#include "Activity.hpp"
#include "Configuration.hpp"
#include "Configuration/Parser.hpp"


std::vector<int> probabilities;
int totalProbability;

void calculateProbabilities()
{
    totalProbability = 0;
    for( auto&& a : config::toMap( config::getOption( "Activities" ) ) )
    {
        int prob = atoi(
                (config::toMap( a.second ).find( "prob" )->second).c_str()
                   );

        probabilities.push_back( prob );
        totalProbability += prob;
    }
}

std::string randomActivityIndex()
{
    int rVal = rand()%totalProbability;
    int i = 0, sum = 0;

    for( auto a : config::toMap( config::getOption( "Activities" ) ) )
    {
        sum += probabilities.at( i );
        if( sum >= rVal ) return a.first;

        i++;
    }

    return NULL;
}

Activity* activityFromOption( std::string aName
                            , bool& skipDelay
                            , bool& regenerate
                            )
{
    std::pair<std::string,std::string> description =
        *(config::toMap( config::getOption( "Activities" ) ).find( aName ));
    return (new Activity( description, skipDelay, regenerate ) );
}
