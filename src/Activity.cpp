#include <gtkmm.h>
#include <map>

#include "Activity.hpp"
#include "Configuration/Parser.hpp"


Activity::Activity( std::pair<std::string,std::string> description
                  , bool& skipDelay
                  , bool& regenerate
                  )
    : skipDelay ( skipDelay )
    , regenerate( regenerate )
{
    set_title( description.first );
    std::string options = description.second;
    config::replaceVars( options );

    std::map<std::string,std::string> optionMap = config::toMap( options );

    label = Gtk::Label( optionMap.find("text")->second );
    skipDelay  = false;
    regenerate = false;

    set_position( Gtk::WIN_POS_CENTER );
    set_border_width       ( 12 );
    grid.set_row_spacing   ( 12 );
    grid.set_column_spacing(  6 );

    generate_buttons();
    for( auto &b : buttons ) grid.add( b );
    grid.attach_next_to(label,buttons.front(),Gtk::POS_TOP,buttons.size());
    add(grid);

    set_resizable(false);
    show_all_children();
}

Activity::~Activity()
{
}


void Activity::generate_buttons()
{
    buttons.push_back( Gtk::Button( "Activity completed!" ) );
    buttons.back().signal_clicked().connect(
            sigc::mem_fun( *this, &Activity::button_activity_completed )
        );

    buttons.push_back( Gtk::Button( "Generate new activity" ) );
    buttons.back().signal_clicked().connect(
            sigc::mem_fun( *this, &Activity::button_generate_new_activity )
        );

    buttons.push_back( Gtk::Button( "Regenerate activity" ) );
    buttons.back().signal_clicked().connect(
            sigc::mem_fun( *this, &Activity::button_regenerate_activity )
        );
}


void Activity::button_activity_completed()
{
    close();
}

void Activity::button_generate_new_activity()
{
    skipDelay = true;
    close();
}

void Activity::button_regenerate_activity()
{
    regenerate = true;
    close();
}
