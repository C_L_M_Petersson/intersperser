#ifndef ACTIVITY_HPP
#define ACTIVITY_HPP

#include <gtkmm.h>
#include <string>
#include <vector>


class Activity : public Gtk::Window
{
    private:
        bool& skipDelay;
        bool& regenerate;

    public:
        Activity( std::pair<std::string,std::string>, bool&, bool& );
        virtual ~Activity();

    protected:
        Gtk::Grid grid;
        Gtk::Label label;
        std::vector<Gtk::Button> buttons;

        void generate_buttons();

        void button_activity_completed();
        void button_generate_new_activity();
        void button_regenerate_activity();
};

#endif
