#include "Configuration/Parser.hpp"
#include "Configuration/Path.hpp"

#include <iostream>
#include <fstream>
#include <string>


namespace config
{
    std::string content;

    void load( int argc, char* argv[] )
    {
        std::string file;
        try {
            file = config::filePath( argc, argv );
        } catch( const char* msg ) {
            std::cerr << msg << std::endl;
        }

        std::ifstream fp( file );

        fp.seekg( 0,std::ios::end );
        content.reserve( fp.tellg() );
        fp.seekg( 0,std::ios::beg );

        content.assign( (std::istreambuf_iterator<char>(fp))
                      ,  std::istreambuf_iterator<char>()
                      );
        filterComments( content );
    }

    std::string getOption( std::string option )
    {
        int optionIndex = nextInstance( config::content, option, 0 );

        int optionStart = nextInstance( config::content
                                      , "("
                                      , optionIndex
                                      , true )+1;
        int optionEnd   = nextInstance( config::content
                                      , ")"
                                      , optionStart
                                      , true );

        return trim( config::content.substr( optionStart
                                           , optionEnd-optionStart
                                           )
                   );
    }
};
