#include "Activity.hpp"
#include "Activity/Distribution.hpp"
#include "Configuration.hpp"
#include "Configuration/Parser.hpp"

#include <gtkmm.h>


int main( int argc, char *argv[] )
{
    config::load( argc, argv );
    calculateProbabilities();
    srand( time( 0 ) );

    while( true )
    {
        bool skipDelay, regenerate;
        std::string aName = randomActivityIndex();
        do
        {
            Glib::RefPtr<Gtk::Application> app = Gtk::Application::create();
            Activity* a = activityFromOption( aName, skipDelay, regenerate );

            app->run( *a );

            app->quit();
            delete a;
        } while( regenerate );

        if( !skipDelay )
            sleep( config::parseTime( config::getOption( "Delay" ) ) );
    }
}
