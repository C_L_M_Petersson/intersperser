#ifndef CONFIGURATION_PARSER_HPP
#define CONFIGURATION_PARSER_HPP

#include <map>
#include <string>
#include <vector>


namespace config
{
    std::string trim( std::string );
    int nextInstance( std::string
                    , std::string
                    , size_t i = 0
                    , bool escapable = false
                    );
    bool escaped( std::string, size_t i );

    std::vector<std::string> toList( std::string );
    std::map<std::string,std::string> toMap( std::string );

    std::string showTime( int );

    int parseTime( std::string );
    void replaceVars( std::string& );

    void filterComments( std::string& );

    std::string expandEnv( std::string );
}

#endif
