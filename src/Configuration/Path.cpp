#include "Configuration/Parser.hpp"

#include <string>
#include <unistd.h>


namespace config
{
    std::string paths[] = { "$XDG_CONFIG_HOME/intersperser/intersperser.conf"
                          , "$HOME/.config/intersperser/intersperser.conf"
                          , "$HOME/.intersperser.conf"
                          , "/etc/intersperser.conf"
                          };


    std::string filePath( int argc, char* argv[] )
    {
        for( int i = 1; i < argc; i++ )
        {
            std::string tmpStr = argv[i];

            if( tmpStr.compare( "-C" ) == 0 )
            {
                if( i+1 < argc )
                {
                    std::string fp = config::expandEnv( argv[i+1] );
                    if( access( fp.c_str(), F_OK ) != -1 )
                        return fp;
                    else
                        throw "File specified with -C not found.";
                } else {
                    throw "Flag -C not followed by a filename!";
                }
            }

            if( tmpStr.compare( 0, 14, "--config-file=" ) == 0 )
            {
                tmpStr.erase( 0, 14 );

                if( tmpStr.empty() )
                    throw "Flag --config-file= not followed by a filename!";

                std::string fp = config::expandEnv( tmpStr );
                if( access( fp.c_str(), F_OK ) != -1 )
                    return fp;
                else
                    throw "File specified with --config-file not found.";
            }
        }

        for( auto &&p: paths )
        {
            std::string fp = config::expandEnv( p );
            if( access( fp.c_str(), F_OK ) != -1 ) return fp;
        }

        throw "No configuration file found!";
    }
};
