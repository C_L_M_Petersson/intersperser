#include "Configuration/Parser.hpp"

#include <boost/algorithm/string/replace.hpp>
#include <fstream>
#include <map>
#include <regex>
#include <string>
#include <vector>


std::string bra = { '{', '[', '(' };
std::string ket = { '}', ']', ')' };


namespace config
{
    std::string trim( std::string str )
    {
        std::string res;

        res = str.erase( 0, str.find_first_not_of(" \f\n\r\t\v")    );
        res = res.erase(    str.find_last_not_of (" \f\n\r\t\v") + 1 );

        return res;
    }

    int nextInstance( std::string str
                    , std::string key
                    , size_t i
                    , bool escapable
                    )
    {
        while( i < str.size() )
        {
            if( str.substr(i,key.length()) == key
                && !(escapable && config::escaped( str, i )) )
            {
                return i;
            }

            size_t j = bra.find(str.at(i));
            if( j != std::string::npos )
            {
                i = config::nextInstance( str, {ket[j]}, i+1, true );
                if( i > str.length() ) i = str.length()-1;
            }

            i ++;
        }

        return -1;
    }

    bool escaped( std::string str, size_t i )
    {
        if( i==0 )
            return false;
        else if( str.at( i-1 )!='\\' )
            return false;
        else if( escaped( str, i-1 ) )
            return false;
        else
            return true;
    }


    std::vector<std::string> toList( std::string str )
    {
        std::vector<std::string> list;

        std::string tmpStr = trim( str );
        if( bra.find(tmpStr.at(0)) != std::string::npos )
        {
            tmpStr.erase(0,1);
            tmpStr.pop_back();
        }

        int i0 = 0, i1;
        do
        {
            i1 = config::nextInstance( tmpStr, ",", i0, true );
            list.push_back( tmpStr.substr( i0, i1-i0 ) );

            i0 = i1+1;

        } while( i0>0 );

        return list;
    }

    std::map<std::string,std::string> toMap( std::string str )
    {
        std::map<std::string,std::string> map;
        std::vector<std::string> list = toList( str );

        for ( auto it = list.begin(); it != list.end(); it++ ) {

            size_t iEq = config::nextInstance( *it, "=", 0, true );

            std::string key = trim((*it).substr( 0, iEq ));
            std::string val = trim((*it).substr( iEq+1 ));

            map.insert( { key, val } );
        }

        return map;
    }


    std::string showTime( int s )
    {
        std::string str;
        int h, m;

        m = s/60;
        s = s%60;

        h = m/60;
        m = m%60;

        if( h!=0 ) str.append( std::to_string( h )+"h" );
        if( m!=0 ) str.append( std::to_string( m )+"m" );
        if( s!=0 ) str.append( std::to_string( s )+"s" );

        if( str.empty() )
        {
            return "0s";
        } else {
            return str;
        }

    }


    int parseInt( std::string str )
    {
        switch( str.front() )
        {
            case '{':
            {
                std::vector<std::string> set = toList( str );

                return std::atoi( set.at( rand()%set.size() ).c_str() );
            }
            case '[':
            {
                std::vector<std::string> range = toList( str );

                int min = std::atoi( range.front().c_str() ),
                    max = std::atoi( range.back ().c_str() );

                return rand()%( max+1-min ) + min;
            }
            default:
                return std::atoi( str.c_str() );
        }
    }

    int parseTime( std::string str )
    {
        switch( str.front() )
        {
            case '{':
            {
                std::vector<std::string> set = toList( str );

                return parseTime( set.at( rand()%set.size() ).c_str() );
            }
            case '[':
            {
                std::vector<std::string> range = toList( str );

                int min = parseTime( range.front().c_str() ),
                    max = parseTime( range.back ().c_str() );

                return rand()%( max+1-min ) + min;
            }
            default:
                int time = 0;

                std::string tmpStr;
                std::smatch match;

                if( std::regex_search( str, match, std::regex("\\d+s") ) )
                {
                    tmpStr = *( match.begin() );
                    tmpStr.pop_back();
                    time = std::atoi( tmpStr.c_str() );
                }

                if( std::regex_search( str, match, std::regex("\\d+m") ) )
                {
                    tmpStr = *( match.begin() );
                    tmpStr.pop_back();
                    time = time + 60*std::atoi( tmpStr.c_str() );
                }

                if( std::regex_search( str, match, std::regex("\\d+h") ) )
                {
                    tmpStr = *( match.begin() );
                    tmpStr.pop_back();
                    time = time + 3600*std::atoi( tmpStr.c_str() );
                }

                return time;
        }
    }

    void replaceVars( std::string &options )
    {
        std::map<std::string,std::string> optionMap
            = config::toMap( options );

        int i = 0;
        std::string key = "i0", val;
        while( optionMap.count( key ) )
        {
            val = optionMap.find( key )->second;
            val = std::to_string( config::parseInt( val ) );

            boost::replace_all( options, "%"+key, val );

            i++;
            key = "i" + std::to_string( i );
        }

        i = 0;
        key = "t0";
        while( optionMap.count( key ) )
        {
            val = optionMap.find( key )->second;
            val = config::showTime( config::parseTime( val ) );

            boost::replace_all( options, "%"+key, val );

            i++;
            key = "i" + std::to_string( i );
        }
    }


    void filterComments( std::string& str )
    {
        std::string tmpStr = "";
        std::stringstream ss(str);

        std::string line;
        while( std::getline( ss, line, '\n' ) )
        {
            size_t i = line.rfind( "#" );
            while( i!=std::string::npos )
            {
                if( !config::escaped( line, i ) )
                    line.erase( i );
                i = line.rfind( "#", i-1 );
            }
            tmpStr.append( line+"\n" );
        }

        str = tmpStr;
        str.pop_back();
    }


    std::string getenvSafe( const std::string & str ) {

         const char * val = std::getenv( str.c_str() );
         if( val == nullptr )
             return "";
         else
             return val;
    }

    bool envVarCharacter( char c )
    {
        return isalpha( c ) || isdigit( c ) || c=='_';
    }

    std::string expandEnv( std::string inStr )
    {
        std::string newStr = "";
        std::string tmpStr;

        while( inStr.length() )
        {
            if( inStr.front() == '$' )
            {
                size_t i = 1;
                while( envVarCharacter( inStr.at(i) ) && i < inStr.length() )
                    i++;

                newStr.append( getenvSafe( inStr.substr( 1,i-1 ).c_str() ) );
                inStr.erase( 0,i );

            } else {

                newStr.append( { inStr.front() } );
                inStr.erase( 0, 1 );

            }
        }
        return newStr;
    }
};
