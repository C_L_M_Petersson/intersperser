#ifndef CONFIGURATION_PATH_HPP
#define CONFIGURATION_PATH_HPP


namespace config
{
    std::string filePath( int, char*[] );
};

#endif
