#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP


namespace config
{
    void load( int, char*[] );
    std::string getOption( std::string );
};

#endif
