# {{{ General settings

all:
	make $(EXECUTABLE)

EXECUTABLE=intersperser

SRCDIR=src/
INCLUDEDIR=/usr/include/
SOURCE=$(shell find src/ -name \*.cpp)

GCC=g++
FLAGS=-Wall -O2
INCLUDE=-Isrc/ $(shell pkg-config --cflags --libs gtkmm-3.0)

# }}}

# {{{ Compiling

$(EXECUTABLE): $(SOURCE)
	$(GCC) -o $@ $(FLAGS) $(INCLUDE) $^

%/:
	mkdir -p $@

clean:
	rm $(EXECUTABLE)

# }}}
